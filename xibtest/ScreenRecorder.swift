import Foundation
import AVFoundation
import Cocoa

final class ScreenRecorder: NSObject, AVCaptureFileOutputRecordingDelegate {

    private var activeContinuation: CheckedContinuation<Void, Never>?
    private var session: AVCaptureSession?
    private var output: AVCaptureMovieFileOutput?

    override init() {
        super.init()
    }

    func start(cropRect: CGRect, outPath: URL) async {
        let session = AVCaptureSession()
        session.sessionPreset = AVCaptureSession.Preset.high
        
        let displayID = CGDirectDisplayID(CGMainDisplayID())
        let input: AVCaptureScreenInput = AVCaptureScreenInput(displayID: displayID)!
        input.capturesCursor = false
        input.capturesMouseClicks = false
        input.cropRect = cropRect
        if session.canAddInput(input) {
            session.addInput(input)
        }
        
        let output = AVCaptureMovieFileOutput()
        if session.canAddOutput(output) {
            session.addOutput(output)
        }
        self.output = output
        
        session.startRunning()
        self.session = session
        
        output.startRecording(to: outPath, recordingDelegate: self)
        return await withCheckedContinuation({ continuation in
            activeContinuation = continuation
            output.startRecording(to: outPath, recordingDelegate: self)
        })
    }

    func stop() {
        output?.stopRecording()
        session?.stopRunning()
    }
    
    func fileOutput(_ output: AVCaptureFileOutput, didStartRecordingTo fileURL: URL, from connections: [AVCaptureConnection]) {
        debugPrint("record started! \(fileURL.path)")
        self.activeContinuation?.resume()
        self.activeContinuation = nil
    }
    
    func fileOutput(_ output: AVCaptureFileOutput, didFinishRecordingTo outputFileURL: URL, from connections: [AVCaptureConnection], error: Error?) {
    }
}
