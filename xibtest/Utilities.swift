import Cocoa
import Foundation

extension CGImage {
    var size: CGSize {
        return CGSize(width: width, height: height)
    }
    var toNSImage: NSImage {
        return NSImage(cgImage: self, size: size)
    }
}

extension NSView {
    /**
    Get a subview matching a condition.
    */
    func firstSubview(deep: Bool = false, where matches: (NSView) -> Bool) -> NSView? {
        for subview in subviews {
            if matches(subview) {
                return subview
            }

            if deep, let match = subview.firstSubview(deep: deep, where: matches) {
                return match
            }
        }

        return nil
    }
}

extension NSObject {
    // Note: It's intentionally a getter to get the dynamic self.
    /**
    Returns the class name without module name.
    */
    static var simpleClassName: String { String(describing: self) }

    /**
    Returns the class name of the instance without module name.
    */
    var simpleClassName: String { Self.simpleClassName }
}

extension NSLayoutConstraint {
    /**
    Returns copy of the constraint with changed properties provided as arguments.
    */
    func changing(
        firstItem: Any? = nil,
        firstAttribute: Attribute? = nil,
        relation: Relation? = nil,
        secondItem: NSView? = nil,
        secondAttribute: Attribute? = nil,
        multiplier: Double? = nil,
        constant: Double? = nil
    ) -> Self {
        .init(
            item: firstItem ?? self.firstItem as Any,
            attribute: firstAttribute ?? self.firstAttribute,
            relatedBy: relation ?? self.relation,
            toItem: secondItem ?? self.secondItem,
            attribute: secondAttribute ?? self.secondAttribute,
            // The compiler fails to auto-convert to CGFloat here.
            multiplier: multiplier.flatMap(CGFloat.init) ?? self.multiplier,
            constant: constant.flatMap(CGFloat.init) ?? self.constant
        )
    }
}
