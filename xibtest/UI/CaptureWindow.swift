import Foundation
import Cocoa

class CaptureWindow: NSWindow {

    @IBOutlet var recordingButton: NSButton!
    @IBOutlet var captureRectView: CaptureArea!
    
    private var recording = false
    private let recorder = ScreenRecorder()
    private var recordingURL: URL?
    private var editVideoWindowController: EditVideoWindowController?
    
    private let defaultBackgroundColor = NSColor(red: 1, green: 0, blue: 0, alpha: 0.1)
    
    override func awakeFromNib() {
        level = .floating
        isOpaque = false
        backgroundColor = defaultBackgroundColor
        isMovableByWindowBackground = true
        let ta = NSTrackingArea(rect: CGRect.zero, options: [.activeAlways, .inVisibleRect, .mouseEnteredAndExited], owner: self, userInfo: nil)
        self.captureRectView.addTrackingArea(ta)
        super.awakeFromNib()
    }
    
    override func mouseEntered(with event: NSEvent) {
        ignoresMouseEvents = true
    }
    
    override func mouseExited(with event: NSEvent) {
        ignoresMouseEvents = false
    }
    
    @IBAction func recordButtonClick(_ sender: Any) {
        // let url = URL(fileURLWithPath: "\(FileManager.SearchPathDirectory.moviesDirectory)/test.mp4")
        // editVideo(url)
        // return
        
        if !recording {
            startRecord()
        } else {
            stopRecord()
        }
    }
    
    func startRecord() {
        let screenRect = captureScreenRect()
        let filename = "\(NSDate().timeIntervalSince1970)"
        let outPath = URL(fileURLWithPath: "\(FileManager.SearchPathDirectory.moviesDirectory)/\(filename).mp4")
        
        Task {
            await recorder.start(cropRect: screenRect, outPath: outPath)
            // UI
            recordingButton.isEnabled = true
            recordingButton.title = "Stop"
        }
        recording = true
        recordingURL = outPath
        
        // UI
        isMovable = false
        recordingButton.isEnabled = false
        recordingButton.title = "Starting..."
        backgroundColor = NSColor.clear
    }
    
    func stopRecord() {
        recorder.stop()
        recording = false
        editVideo(url: recordingURL!)
        
        // UI
        recordingButton.title = "Record"
        backgroundColor = defaultBackgroundColor
        isMovable = true
    }
    
    func editVideo(url: URL) {
        let editController = EditVideoWindowController()
        editController.loadVideo(url: url)
        editController.showWindow(self)
    }
    
    func captureScreenRect() -> CGRect {
        let offset: CGFloat = 3
        let outer = frame
        let inner = captureRectView.frame
        return CGRect(
            x: outer.minX + inner.minX + offset,
            y: outer.minY + inner.minY + offset,
            width: inner.width - (offset*2),
            height: inner.height - (offset*2))
    }
}
