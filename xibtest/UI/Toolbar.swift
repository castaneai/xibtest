import Cocoa

class Toolbar: NSView {
    @IBInspectable var backgroundColor: NSColor? {
        get {
            guard let backgroundColor = layer?.backgroundColor else {
                return nil
            }
            return NSColor(cgColor: backgroundColor)
        }
        set {
            wantsLayer = true
            layer?.backgroundColor = newValue?.cgColor
        }
    }
}
