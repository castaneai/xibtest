import Cocoa
import AVKit
import Combine

final class EditVideoWindowController: NSWindowController, NSWindowDelegate {
    
    override var windowNibName: NSNib.Name? {
        return "EditVideoWindow"
    }
    
    private var asset: AVAsset!
    
    override func windowDidLoad() {
        super.windowDidLoad()
        let window = self.window as! EditVideoWindow
        let video = asset.tracks(withMediaType: .video).first
        debugPrint("---------- load video: \(String(describing: video))")
        
        let playerItem = AVPlayerItem(asset: asset)
        let player = AVPlayer(playerItem: playerItem)
        let playerView = window.playerView
        playerView?.player = player
        playerView?.setupTrimmingObserver()
    }

    func loadVideo(url: URL) {
        self.asset = AVURLAsset(url: url)
    }

}

class EditVideoWindow: NSWindow {
    @IBOutlet var playerView: TrimmingAVPlayerView!
    
    override func keyDown(with event: NSEvent) {
        // 8: 'c' key
        if event.keyCode == 8 {
            let time = playerView.player!.currentTime()
            let imageGenerator = AVAssetImageGenerator(asset: playerView.player!.currentItem!.asset)
            do {
                let image = (try imageGenerator.copyCGImage(at: time, actualTime: nil)).toNSImage
                debugPrint("copy image: \(image)")
                let pb = NSPasteboard.general
                pb.clearContents()
                pb.writeObjects([image])
            } catch {
                // TODO: error
            }
        }
    }
}

class TrimmingAVPlayerView: AVPlayerView {
    private var trimmingCancellable: AnyCancellable?
    
    fileprivate func setupTrimmingObserver() {
        trimmingCancellable = publisher(for: \.canBeginTrimming, options: .new)
            .sink { [weak self] canBeginTrimming in
                guard
                    let self = self,
                    canBeginTrimming
                else {
                    return
                }

                self.beginTrimming(completionHandler: nil)
                self.hideTrimButtons()
                self.trimmingCancellable = nil
            }
    }
    
    // ref: https://github.com/sindresorhus/Gifski/blob/9a8805ed6392748d9d699a78fbba39e0e77cf64e/Gifski/TrimmingAVPlayerViewController.swift#L172
    fileprivate func hideTrimButtons() {
        // This method is a collection of hacks, so it might be acting funky on different OS versions.
        guard
            let avTrimView = firstSubview(deep: true, where: { $0.simpleClassName == "AVTrimView" }),
            let superview = avTrimView.superview
        else {
            return
        }

        // First find the constraints for `avTrimView` that pins to the left edge of the button.
        // Then replace the left edge of a button with the right edge - this will stretch the trim view.
        if let constraint = superview.constraints.first(where: {
            ($0.firstItem as? NSView) == avTrimView && $0.firstAttribute == .right
        }) {
            superview.removeConstraint(constraint)
            constraint.changing(secondAttribute: .right).isActive = true
        }

        if let constraint = superview.constraints.first(where: {
            ($0.secondItem as? NSView) == avTrimView && $0.secondAttribute == .right
        }) {
            superview.removeConstraint(constraint)
            constraint.changing(firstAttribute: .right).isActive = true
        }

        // Now find buttons that are not images (images are playing controls) and hide them.
        superview.subviews
            .first { $0 != avTrimView }?
            .subviews
            .filter { ($0 as? NSButton)?.image == nil }
            .forEach {
                $0.isHidden = true
            }
    }
}
